class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        //让第一个数组的长度小于等于数组2
        if(nums1.size() > nums2.size()){
            vector<int> temp = nums1;
            nums1 = nums2;
            nums2 = temp;
        }
        int m = nums1.size();
        int n = nums2.size();
        int total_l = (m + n + 1) / 2;
        int l = 0, r = m;
        while(l < r){
            int i = (l + r + 1) / 2;
            int j = total_l - i;
            if(nums1[i - 1] > nums2[j]) r = i -1;
            else l = i;
        }
        int i = l;
        int j = total_l - i;
        int nums1_l_min = i == 0 ? INT_MIN : nums1[i - 1];
        int nums1_r_max = i == m ? INT_MAX : nums1[i];
        int nums2_l_min = j == 0 ? INT_MIN : nums2[j - 1];
        int nums2_r_max = j == n ? INT_MAX : nums2[j];
        if((m + n) % 2 == 1) return max(nums1_l_min, nums2_l_min);
        else return (double) (max(nums1_l_min, nums2_l_min) + min(nums1_r_max, nums2_r_max))/2.0;

    }
};