#include <iostream>
#include <vector>
#include <map>

using namespace std;

vector<int> twoSum_baoli(vector<int>& nums, int target);
vector<int> twoSum_hash(vector<int>& nums, int target);

int main()
{
    vector<int> nums(4);
    for(int i = 0; i < 4; i++){
        cin >> nums[i];
    }
    vector<int> res = twoSum_hash(nums, 9);
    for(int i = 0; i < 2; i++){
        cout << res[i] << " ";
    }
    return 0;
}

vector<int> twoSum_baoli(vector<int>& nums, int target){
    int len = nums.size();
    vector<int> res(2, -1);
    for(int i = 0; i < len - 1; i++){
        for(int j = i + 1; j < len; i++){
            if(nums[i] + nums[j] == target){
                res[0] = i;
                res[1] = j;
                return res;
            }
        }
    }
    return res;
}

vector<int> twoSum_hash(vector<int>& nums, int target){
    int len = nums.size();
    vector<int> res(2, -1);
    map<int, int> mp;
    for(int i = 0; i < len; i++){
        mp.insert(make_pair(nums[i], i));
    }
    for(int i = 0; i < len; i++){
        if( mp.count(target - nums[i])>0 && mp[target - nums[i]] != i ){
            res[0] = i;
            res[1] = mp[target - nums[i]];
            return res;
        }
    }
    return res;
}
