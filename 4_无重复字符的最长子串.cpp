#include <iostream>
#include <string>
#include <map>


using namespace std;

int lengthOfLongestSubstring(string s);

int main()
{
    string s;
    cin >> s;
    cout<<s<<endl;
    int res = lengthOfLongestSubstring(s);
    cout <<"res="<< res <<endl;
    return 0;
}

int lengthOfLongestSubstring(string s) {
        int len = s.size();
        cout<<"len = "<<len<<endl;
        map<char, int> mp;
        int res = 0;
        for(int st = 0, ed = 0; ed < len; ed++){
            char ch = s[ed];
            if(mp.find(ch) != mp.end()){
                st = max(mp[ch] + 1, st);
                mp[ch] = ed;
                cout<<"st = "<<st<<endl;
            }else{
                mp[ch] = ed;
                cout<<"ed="<<ed<<endl;

            }
            res = max(res, ed - st + 1);
        }
        return res;
    }
