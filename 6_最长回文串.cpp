class Solution {
public:
    string longestPalindrome(string s) {
        int len = s.size();
        if(len < 1) return "";
        int left = 0, right =0;
        int max_index = 0;
        int res = 0;
        int h_len = 1;
        for(int i = 0; i < len; i++){
            left = i - 1;
            right = i + 1;
            while(left >= 0 && s[left] == s[i]){
                h_len++;
                left--;
            }
            while(right < len && s[i] ==s[right]){
                h_len++;
                right++;
            }
            while(left >= 0 && right < len && s[left] == s[right]){
                h_len = h_len + 2;
                left--;
                right++;
            }
            if(h_len > res){
                res = h_len;
                max_index = left;
            }
            h_len = 1;
        }
        return s.substr(max_index + 1, res);
    }
};