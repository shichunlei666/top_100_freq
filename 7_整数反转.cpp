class Solution {
public:
    int reverse(int x) {
        vector<int> res;
        bool zheng = true;
        if(x < 0) zheng = false;
        x = abs(x);
        while(x){
            res.push_back(x % 10);
            x /= 10;
        }
        long num = 0;
        for(auto & n : res){
            long temp = num*10 + n;
            if(temp > INT_MAX || temp < INT_MIN) return 0;
            num = temp;
        }
        if(!zheng) return (int)-1*num;
        return num;
    }
};