#include <iostream>
#include <vector>
#include <map>

using namespace std;

int search(vector<int>& nums, int target);

int main()
{
    vector<int> nums(6);
    for(int i = 0; i < 6; i++){
        cin >> nums[i];
    }
    int res = search(nums, 9);

    cout << res << " ";
    return 0;
}

int search(vector<int>& nums, int target){
    int len = nums.size();
    int left = 0;
    int right = len - 1;
    while(left <= right){
        int mid = (left + right) / 2;
        if(nums[mid] > target){
            right = mid - 1;
        }else if(nums[mid] < target){
            left = mid + 1;
        }else{
            return mid;
        }
    }
    return -1;
}
